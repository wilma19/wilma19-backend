'use strict';

/**
 * wilma-19-page router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wilma-19-page.wilma-19-page');
