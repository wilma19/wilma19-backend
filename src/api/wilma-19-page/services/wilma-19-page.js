'use strict';

/**
 * wilma-19-page service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wilma-19-page.wilma-19-page');
