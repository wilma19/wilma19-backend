'use strict';

/**
 * wilma-19-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wilma-19-page.wilma-19-page');
