'use strict';

/**
 * plattenkosmos-post service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::plattenkosmos-post.plattenkosmos-post');
