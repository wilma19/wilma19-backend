'use strict';

/**
 * plattenkosmos-page router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::plattenkosmos-page.plattenkosmos-page');
