'use strict';

/**
 * plattenkosmos-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::plattenkosmos-page.plattenkosmos-page');
