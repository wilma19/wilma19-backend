'use strict';

/**
 * plattenkosmos-page service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::plattenkosmos-page.plattenkosmos-page');
