'use strict';

/**
 * wilma-19-post service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wilma-19-post.wilma-19-post');
