'use strict';

/**
 * wilma-19-post controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wilma-19-post.wilma-19-post');
