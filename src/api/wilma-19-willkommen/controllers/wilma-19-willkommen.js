'use strict';

/**
 * wilma-19-willkommen controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wilma-19-willkommen.wilma-19-willkommen');
