'use strict';

/**
 * plattenkosmos-recurring-event service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::plattenkosmos-recurring-event.plattenkosmos-recurring-event');
