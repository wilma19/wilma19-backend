'use strict';

/**
 * plattenkosmos-recurring-event controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::plattenkosmos-recurring-event.plattenkosmos-recurring-event');
