'use strict';

/**
 * plattenkosmos-recurring-event router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::plattenkosmos-recurring-event.plattenkosmos-recurring-event');
