// plugins.js
module.exports = () => {
  return {
    ckeditor: {
     enabled: true,
     config:{
        editor:{
          // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html
          toolbar: {
            items: [
              'heading',
              '|',
              'bold',
              'italic',
              'underline',
              'removeFormat',
              '|',
              'highlight:yellowMarker',
              'removeHighlight',
              '|',
              'alignment:center',
              '|',
              'bulletedList',
              'numberedList',
              '|',
              'horizontalLine',
              '|',
              'StrapiMediaLib',
              'blockQuote',
              'link',
              '|',
              'htmlEmbed',
              'specialCharacters',
              '|',
              "fullScreen",
              'undo',
              'redo'
            ]
          },
          highlight: {
            options: [
                {
                    model: 'yellowMarker',
                    class: 'marker-yellow',
                    title: 'Yellow purple highlight',
                    color: 'var(--ck-highlight-marker-yellow)',
                    type: 'marker'
                },
            ]
          },
          // https://ckeditor.com/docs/ckeditor5/latest/features/images/images-overview.html
          image: {
            resizeOptions: [],
            toolbar: []
          },
          // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html
          heading: {
            options: [
              { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
              { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
              { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
              { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
              { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
            ]
          },
          // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html
          // Regular expressions (/.*/  /^(p|h[2-4])$/' etc) for htmlSupport does not allowed in this config
          htmlSupport: {
            allow: [
                {
                  name: 'img',
                  attributes: {
                      sizes:true,
                      loading:true,
                  }
                },
            ]
          },
        }
      }
    }
  }
}
